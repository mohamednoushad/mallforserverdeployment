const express = require('express');
const common = require('../lib/common');
const { restrict, checkAccess } = require('../lib/auth');
const escape = require('html-entities').AllHtmlEntities;
const colors = require('colors');
const bcrypt = require('bcryptjs');
const moment = require('moment');
const fs = require('fs');
const path = require('path');
const multer = require('multer');
const mime = require('mime-type/with-db');
const csrf = require('csurf');
const { validateJson } = require('../lib/schema');
const ObjectId = require('mongodb').ObjectID;
const router = express.Router();
const csrfProtection = csrf({ cookie: true });

// Regex
const emailRegex = /\S+@\S+\.\S+/;
const numericRegex = /^\d*\.?\d*$/;



router.get('/wallet/walletpage', (req, res) => {
    res.render('wallet', {title: 'Wallet', config: req.app.config, helpers: req.handlebars.helpers, session: req.session, message: common.clearSessionValue(req.session, 'message'),messageType: common.clearSessionValue(req.session, 'messageType')});

});




module.exports = router;
