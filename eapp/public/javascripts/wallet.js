var dataTimer = null;
let currentAddress;
var dividendValue = 0;
var tokenBalance = 0;
let actual_JSON;
var ethPrice = 0;
var ethPriceTimer = null;
let ethTaiwanPrice = 0;
let ethTaiwanPriceTimer = null;
var currency = (typeof default_currency === 'undefined') ? 'USD' : default_currency;
var buyPrice = 0;
var globalBuyPrice = 0;
let contract;
var sellPrice = 0;
var infoTimer = null;
let masterNodeLink;



function convertWeiToEth(e) {
  return e / 1e18
}


function convertEthToWei(e) {
  return 1e18 * e
}

const abi = [{
  "constant": false,
  "inputs": [],
  "name": "buy",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": true,
  "stateMutability": "payable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_newTokenAmount",
    "type": "uint256"
  }],
  "name": "changeTokensForOneEther",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [],
  "name": "ownerAndFriendsWithdrawDividends",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_identifier",
    "type": "address"
  }, {
    "name": "_status",
    "type": "bool"
  }],
  "name": "setAdministrator",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_name",
    "type": "string"
  }],
  "name": "setName",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_amountOfTokens",
    "type": "uint256"
  }],
  "name": "setStakingRequirement",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_symbol",
    "type": "string"
  }],
  "name": "setSymbol",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [{
    "name": "_toAddress",
    "type": "address"
  }, {
    "name": "_amountOfTokens",
    "type": "uint256"
  }],
  "name": "transfer",
  "outputs": [{
    "name": "",
    "type": "bool"
  }],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "constant": false,
  "inputs": [],
  "name": "withdraw",
  "outputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "function"
}, {
  "inputs": [],
  "payable": false,
  "stateMutability": "nonpayable",
  "type": "constructor"
}, {
  "payable": true,
  "stateMutability": "payable",
  "type": "fallback"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "name": "customerAddress",
    "type": "address"
  }, {
    "indexed": false,
    "name": "incomingEthereum",
    "type": "uint256"
  }, {
    "indexed": false,
    "name": "tokensMinted",
    "type": "uint256"
  }, {
    "indexed": true,
    "name": "referredBy",
    "type": "address"
  }],
  "name": "onTokenPurchase",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "name": "customerAddress",
    "type": "address"
  }, {
    "indexed": false,
    "name": "tokensBurned",
    "type": "uint256"
  }, {
    "indexed": false,
    "name": "ethereumEarned",
    "type": "uint256"
  }],
  "name": "onTokenSell",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "name": "customerAddress",
    "type": "address"
  }, {
    "indexed": false,
    "name": "ethereumWithdrawn",
    "type": "uint256"
  }],
  "name": "onWithdraw",
  "type": "event"
}, {
  "anonymous": false,
  "inputs": [{
    "indexed": true,
    "name": "from",
    "type": "address"
  }, {
    "indexed": true,
    "name": "to",
    "type": "address"
  }, {
    "indexed": false,
    "name": "tokens",
    "type": "uint256"
  }],
  "name": "Transfer",
  "type": "event"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "administrators",
  "outputs": [{
    "name": "",
    "type": "bool"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "adminsAlreadyConvertedTokensToEth",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_customerAddress",
    "type": "address"
  }],
  "name": "balanceOf",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_ethereumToSpend",
    "type": "uint256"
  }],
  "name": "calculateTokensReceived",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "decimals",
  "outputs": [{
    "name": "",
    "type": "uint8"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "uint256"
  }],
  "name": "ethInTheStage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "hasUserPurchasedBefore",
  "outputs": [{
    "name": "",
    "type": "bool"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "highestEthContractHaveSeen",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "lastPurchasedUser",
  "outputs": [{
    "name": "",
    "type": "address"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "minimumPurchaseToken",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "myTokens",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "name",
  "outputs": [{
    "name": "",
    "type": "string"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_ownerOrFriendsAddress",
    "type": "address"
  }],
  "name": "OwnerAndFriendsSeeTheirTokensInPool",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "ownerFriendOneSharePercentage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "ownerFriendTwoSharePercentage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_adminAddress",
    "type": "address"
  }],
  "name": "OwnersAndFriendsSeeDividends",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "ownerSharePercentage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "payoutsTo_",
  "outputs": [{
    "name": "",
    "type": "int256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "_customerAddress",
    "type": "address"
  }],
  "name": "seeMyDividend",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "sharePercentagesOfOwnerOrFriends",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "stage",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "stageOfInitialPurchaseOfUser",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "symbol",
  "outputs": [{
    "name": "",
    "type": "string"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "tokenPool",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "tokensForOneEther",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "tokenSupply_",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "totalEthereumBalance",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [],
  "name": "totalSupply",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "userlastPurchasedToken",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}, {
  "constant": true,
  "inputs": [{
    "name": "",
    "type": "address"
  }],
  "name": "userTotalPurchasedTokens",
  "outputs": [{
    "name": "",
    "type": "uint256"
  }],
  "payable": false,
  "stateMutability": "view",
  "type": "function"
}]
const contractAddress = "0x9224cfb6c40753c573e2b1daf0f7f678cab3749b";


const App = {
  web3: null,
  account: null,
  meta: null,

  start: async function () {
    const {
      web3
    } = this;

    console.log("App Started");

    try {

      console.log(web3.eth.accounts);

      this.meta = new web3.eth.Contract(
        abi,
        contractAddress,
      );

      // get accounts
      const accounts = await web3.eth.getAccounts();
      this.account = accounts[0];

      // this.updateEthPrice();
      this.refreshTable();
    } catch (error) {
      $('#userAddress').html("cannot be obtained. Please connect Metamask");
      console.error("Could not connect to contract or chain.");
    }
  },

  refreshTable: async function () {

    const {
      balanceOf
    } = this.meta.methods;
    const userTokenBalanceFromContract = await balanceOf(this.account).call();

    $('#userTokenBalance').html(userTokenBalanceFromContract / 100);


  },

  makePaymentInTokens: async function () {

    const {
      transfer
    } = this.meta.methods;

    let address = "0x581A5B779f12bD13C6f63ED0CCb6c32A734F0951";
    let amount = $("#metamaskButton").data("amount").toString();

    try {

      if (web3.isAddress(address) && parseFloat(amount)) {
        var amountConvertedToCoverDecimalPlaces = amount * 100;
        $("#trial").show();
        const result = await transfer(address, amountConvertedToCoverDecimalPlaces.toFixed(0)).send({
          from: this.account
        });


        console.log(result);

        if (result.status) {
          this.refreshTable();
          return result.transactionHash;
        } else {
          return false;
        }

      } else {
        console.log("Invalid Address / Amount");
        $("#errorMsg").html("Invalid Address / Amount");
        $(".popup-overlay, .popup-content").addClass("active");
      }

    } catch (error) {

      console.log(error);
      return false;

    }


  }


}


window.App = App;

$(document).ready(function () {

  $("#trial").hide();


  if (window.ethereum) {
    // use MetaMask's provider
    App.web3 = new Web3(window.ethereum);
    `  `
    window.ethereum.enable(); // get permission to access accounts
    console.log("using metamask");
    // console.log(web3.eth.accounts);
    // window.ethereum.on('accountsChanged', function (accounts) {
    //   location.reload(); 
    // })
    // window.ethereum.on('networkChanged', function (accounts) {
    //   location.reload(); 
    // })
  } else {
    console.warn(
      "No web3 detected. Falling back to http://127.0.0.1:8545. You should remove this fallback when you deploy live",
    );
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    App.web3 = new Web3(
      new Web3.providers.HttpProvider("http://127.0.0.1:8545"),
    );
  }

  App.start();

  $("#tokensToEstimateEther").keyup(function () {

    let userInputTokensForEstimate = $("#tokensToEstimateEther").val();
    console.log(tokensFor1Eth);
    console.log(userInputTokensForEstimate);
    var approximateEtherForTokens = +(userInputTokensForEstimate) / +(tokensFor1Eth);
    $('#etherForInputTokens').html(approximateEtherForTokens);

  });

  $("#ethersToBuyToken").keyup(function () {

    let userInputEtherForEstimate = $("#ethersToBuyToken").val();
    console.log(tokensFor1Eth);
    console.log(userInputEtherForEstimate);
    var approximateEtherForTokens = +(userInputEtherForEstimate) * +(tokensFor1Eth);
    $('#approximateTokensForInputEther').html(approximateEtherForTokens.toFixed(2));

  });



});